# Obschak

[Smart contract](./obschak/katel.sol) that distributes money based on preset allowances.

## Contributors
* Anton Antonov (ideas, research, useful materials)
* Anton Zalaldinov (ideas, code, code review, debug, research)
* Denis Nikolskiy (code, debug, research)

## Description

When the smart contract is deployed the owner can specify the maximum limit
that can be withdrawn by other people. For example Alice can send money to the
smart contract. When Bob and Tom ask her for money, Alice can allow Bob to 
withdraw 1 eth and Tom 2 eth. They don't have to take it in one chunk. For example 
Tom decides to withdraw only 0.5 eth now. Later Tom will be able to withdraw
only the remaining 1.5 eth. Alice as the owner of the smart contract can 
reset amounts allowed for withdrawal at any time.

## Architecture

The contract contains the address of the `owner`, the payable address of the
contract called `thePot`, and a mapping between payable addresses and `allowance`
that the address can withdraw.

Function `setAllowance` verifies that the caller is the owner of the smart
contract and modifies the `allowance` map.

Function `getMoney` verifies that the requested amount is not bigger than the
allowed amount for the address.

Function `addMoney` allows anyone to send money to `NadaDeneg` smart contract.

Function `getBal` returns available balance in `thePot`.