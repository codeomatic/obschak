// SPDX-License-Identifier: GPL-3.0
pragma solidity >= 0.7.0 <0.9.0;

contract NadaDeneg {

    address public owner;
    address payable thePot;
    mapping (address => uint256) allowance;

    constructor() {
        owner = msg.sender;
    }

    function addMoney() public payable {
        // Anyone can send money to the pot
        thePot.transfer(msg.value);
    }

    function getMoney(uint amount) public payable {
        address payable myAddr = payable(msg.sender);
        uint256 maxWithdraw = allowance[myAddr];

        require(maxWithdraw > 0, "Not allowed to get money from this pot :(");
        require(amount <= maxWithdraw, "You asked more than you can get :(");
        require(amount <= getBal(), "Not enough money in the pot :(");
    
        allowance[myAddr] = maxWithdraw - amount;
        myAddr.transfer(amount);
    }

    function getBal() public view returns(uint){
        return address(thePot).balance;
    }

    function setAllowance(address payable to, uint256 amount) public {
        require(msg.sender == owner, "Only owner can set allowances");
        allowance[to] = amount;
    }
}